Containers
==========

This repository is defining basic environment for development with CentOS traditional style.

Environment structure
---------------------

* yamashita109
    * base-jp:centos72trad
        * web-jp:centos72trad-apache2
            * php-jp:centos72trad-apache2-php54
            * php-jp:centos72trad-apache2-php56
            * php-jp:centos72trad-apache2-php70
        * db-jp:centos72trad-mariadb55
        * db-jp:centos72trad-mariadb100
        * db-jp:centos72trad-mysql56
        * mail-jp:centos72trad-postfix2

