This is a basic web server.

Example of starting a container:
```
# docker volume create --name=web
# docker run --privileged -d \
      -h example.com \
      -v web:/var/www \
      -v ./etc/httpd:/etc/httpd/conf \
      -v ./etc/httpd:/etc/httpd/conf.d \
      -v ./etc/httpd:/etc/httpd/conf.modules.d \
      yamashita109/web-jp:centos7trad-apache2
```
