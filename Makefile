all:
	cd base-jp_centos72						&& make;

	cd web-jp_centos72trad-apache2			&& make;
	cd php-jp_centos72trad-apache2-php54	&& make;
	cd php-jp_centos72trad-apache2-php56	&& make;
	cd php-jp_centos72trad-apache2-php70	&& make;

	cd db-jp_centos72trad-mariadb55			&& make;
	cd db-jp_centos72trad-mariadb100		&& make;
	cd db-jp_centos72trad-mysql56			&& make;

	cd mail-jp_centos72trad-postfix2		&& make;

	cd python2-sphinx						&& make;
