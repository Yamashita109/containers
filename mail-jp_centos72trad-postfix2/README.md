This is a basic mail server.

Example of starting a container:
```
# docker run --privileged -d \
      -h example.com \
      -v ./etc/postfix:/etc/postfix \
      yamashita109/mail-jp:centos72trad-postfix2
```
