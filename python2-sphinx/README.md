Python 2 + Sphinx for Cakephp3 document compiling environment
=============================================================

# Usage

## Initializing your environment

### First. Fork official repo into your git repo

Ref: Official Repo https://github.com/cakephp/docs

###  Second. Clone it into local

```
~/work/ # git clone git@github.com:(your account)/docs.git
~/work/ # cd docs
~/work/docs/ # git branch 3.0 origin/3.0
~/work/docs/ # git checkout 3.0
~/work/docs/ # git remote add official git@github.com:cakephp/docs.git
```

## Translation routine

```
~/work/docs/ # git pull --rebase official 3.0
~/work/docs/ # vim ja/index.rst
~/work/docs/ # docker run --rm -v `pwd`:/var/docs yamashita109/python2-sphinx:latest make html-ja
(open build/html/ja/index.html with your browser)
~/work/docs/ # git push origin 3.0
```

## References

- [CakePHP3の公式ドキュメントを翻訳する方法@Coodip](https://coodip.com/articles/3426)
- [CakePHPドキュメント翻訳グループ@Facebook](https://www.facebook.com/groups/469140879787423/)
- [翻訳ミーティング@connpass](http://cakephp.connpass.com/)

