This is a basic web+php server by individual php version.

Example of starting a container:
```
# docker volume create --name=web
# docker run --privileged -d \
      -h example.com \
      -v web:/var/www \
      -v ./etc/httpd:/etc/httpd/conf \
      -v ./etc/httpd:/etc/httpd/conf.d \
      -v ./etc/httpd:/etc/httpd/conf.modules.d \
      -v ./etc/php.ini:/etc/php.ini \
      -v ./etc/php.d:/etc/php.d \
      yamashita109/php-jp:centos7trad-apache2-php54
```
