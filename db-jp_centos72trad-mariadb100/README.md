This is a basic db server by individual db version.

Example of starting a container:
```
# docker volume create --name=db
# docker run --privileged -d \
      -h example.com \
      -v db:/var/lib/mysql \
      -v ./etc/my.cnf:/etc/my.cnf \
      -v ./etc/my.cnf.d:/etc/my.cnf.d \
      yamashita109/db-jp:centos7trad-mariadb100
```
